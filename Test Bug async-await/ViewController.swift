/*
 * ViewController.swift
 * Test Bug async-await
 *
 * Created by François Lamboley on 10/21/23.
 */

import CoreLocation
import UIKit



class ViewController: UIViewController {
	
	public func setLocationText(from coords: CLLocationCoordinate2D) {
		let location = CLLocation(latitude: coords.latitude, longitude: coords.longitude)
		setLocationText("Loading…") /* <- Cancels the current geocoding if any. */
		currentGeocodingTask = Task{ @MainActor [weak self, geocoder] in
			/* The fallback when geocoder fails is unlocalized on purpose (in Apple Maps, coords are using English number separator, and lat/long separator is a comma). */
			let locationName = (try? await geocoder.reverseGeocodeLocation(location).first?.name) ?? String(format: "%.5f,%.5f", location.coordinate.latitude, location.coordinate.longitude)
			guard !Task.isCancelled else {return}
			
			self?.currentGeocodingTask = nil
			self?.setLocationText(locationName)
		}
	}
	
	public func setLocationText(_ newText: String) {
		currentGeocodingTask?.cancel()
		currentGeocodingTask = nil
		
		label.text = newText
	}
	
	@IBAction private func startTest() {
//		setLocationText(from: .init(latitude: 37.332028, longitude: -122.029627))
//		setLocationText(from: .init(latitude: 37.332028, longitude: -122.029627))
		
		/* Apparently if using a geocoder while already in use, the second call will never terminate.
		 * Tested on iOS 17.5 and 18.2. */
		Task{ @MainActor in
			print("yolo1 - before")
			_ = try? await geocoder.reverseGeocodeLocation(.init(latitude: 37.332028, longitude: -122.029627))
			print("yolo1 - after")
		}
		Task{ @MainActor in
			print("yolo2 - before")
			_ = try? await geocoder.reverseGeocodeLocation(.init(latitude: 37.332028, longitude: -122.029627))
			print("yolo2 - after") /* <- Never called. */
		}
	}
	
	@IBOutlet private var label: UILabel!
	
	private let geocoder = CLGeocoder()
	private var currentGeocodingTask: Task<Void, Never>?
	
}
